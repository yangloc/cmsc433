package support;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import actors.ListManagerActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.pattern.Patterns;
import messages.*;

import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

/**
 * A class containing a main() function that tests some actor-implementation routines.  Feel
 * free to modify for your own testing purposes.  Please do not include any code that other classes
 * may depend on, however; this is strictly for testing your code.
 * 
 * @author Rance Cleaveland
 *
 */
public class Main {
	
	
	public static String getName (ActorRef actor) {
		return (actor.path().name());
	}
	
	public static void main(String[] args) {
		
		ActorSystem listSystem = ActorSystem.create("list_system");
		ActorRef lm = listSystem.actorOf(Props.create(ListManagerActor.class));
		
		// Create list operations object.
		
		IntegerListOperations lops = new IntegerListOperations(lm);

		// Create null actor. "[]" should be printed.
		ActorRef nil = lops.nil();
		System.out.printf("Null list is %s%n", lops.exportList(nil));
		
		System.out.printf("Null list isNull? %s%n", lops.isNull(nil));
		// Cons 3 onto null actor.  "[3]" should be printed.
		ActorRef list3 = lops.cons(3, nil);
		System.out.printf("List3 is %s%n", lops.exportList(list3));
		
		// Get head value.  "3" should be printed.
		System.out.printf("Head of list3 is %d%n", lops.head(list3));

		// Get tail value.  "[]" should be printed.
		ActorRef tailList3 = lops.tail(list3);
		System.out.printf("Tail of list3 is %s%n", lops.exportList(tailList3));
		
		// Cons 2 onto list3. "[2,3]" should be printed.
		ActorRef list23 = lops.cons(2, list3);
		System.out.printf("List23 is %s%n", lops.exportList(list23));
		
		// Get head, tail of list23. "2", then "[3]", should be printed.
		System.out.printf("Head of list 23 is %s; tail of list23 is %s%n", lops.head(list23), lops.exportList(lops.tail(list23)));
		
		// Now do list import / export and compare.  "[1,2,3]" should be printed twice.
		
		List<Integer> l123 = Arrays.asList(1,2,3);
		System.out.printf("Imported l123 is %s%n", l123);
		ActorRef list123 = lops.importList(l123);
		System.out.printf("Exported list123 is %s%n", lops.exportList(list123));
		
		// Now try map. "[]", "[4]" and "[2,3,4]" should be printed.
		
		ActorRef mappedNil = lops.add1(nil);
		System.out.printf("Mapped nullNode is %s%n", lops.exportList(mappedNil));
		ActorRef mappedList3 = lops.add1(list3);
		System.out.printf("Mapped list3 is %s%n", lops.exportList(mappedList3));
		ActorRef mappedList123 = lops.add1(list123);
		System.out.printf("Mapped list123 is %s%n", lops.exportList(mappedList123));
		
		// Now try sum. "0" and "6" should be printed.
		
		System.out.printf("The sum of nil is %d%n", lops.sum(nil));
		System.out.printf("The sum of list123 is %d%n", lops.sum(list123));
		
		listSystem.terminate();
	}

}

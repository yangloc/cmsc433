package messages;

/**
 * Class of messages for requesting whether or not a given list actor corresponds to an empty or
 * non-empty list.
 * 
 * DO NOT CHANGE THIS FILE!
 * 
 * @author Rance Cleaveland
 *
 */
public class NullStatusRequestMessage {

}

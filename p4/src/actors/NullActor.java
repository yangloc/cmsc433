package actors;

import java.util.ArrayList;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import messages.CollectRequestMessage;
import messages.CollectResultMessage;
import messages.NullStatusRequestMessage;
import messages.NullStatusResultMessage;
import support.IntegerCollector;

/**
 * Class of actors implementing null nodes in applicative lists.  Despite the name,
 * these actors have a role to play, specifically in returning results of collect operations
 * and others.
 * 
 * You must complete the implementation of onReceive().  You also add additional private fields if
 * you wish, although you may not change any constructor.
 *
 */
public class NullActor extends UntypedActor {
	
	static public Props props = Props.create(NullActor.class);
	

	@Override
	public void onReceive(Object msg) throws Exception {
		if (msg instanceof NullStatusRequestMessage){
			getSender().tell(new NullStatusResultMessage(true), getSelf());
		}else if(msg instanceof CollectRequestMessage){
			CollectRequestMessage<ArrayList<Integer>> request = (CollectRequestMessage<ArrayList<Integer>>) msg;
			getSender().tell(new CollectResultMessage<ArrayList<Integer>>(request), getSelf());
		}else{
			System.out.printf("Bad message to ListManagerActor:  %s%n", msg.toString());
			unhandled(msg);  // Recommended practice
		}
	}

}

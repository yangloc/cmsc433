package actors;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import messages.ConsActorRequestMessage;
import messages.ConsActorResultMessage;
import messages.HeadRequestMessage;
import messages.HeadResultMessage;
import messages.MapRequestMessage;
import messages.MapResultMessage;
import messages.NullActorRequestMessage;
import messages.NullActorResultMessage;
import messages.NullStatusRequestMessage;
import messages.NullStatusResultMessage;
import messages.StartProcessingMessage;
import messages.TailRequestMessage;
import messages.TailResultMessage;
import support.IntegerCompute;

/**
 * Class of actors for processing map requests (i.e. requests for the creation of a new list
 * obtained by applying a function to each node in an existing list).
 * 
 * You must complete the implementation of onReceive().  You also add additional private fields if
 * you wish, although you may not change any constructor.
 *
 */
public class MapProcessingActor extends UntypedActor {
	
	static public Props props (ActorRef listManager, ActorRef replyTo, MapRequestMessage request) {
		return Props.create(MapProcessingActor.class, listManager, replyTo, request);
	}
	
	private ActorRef listManager;			// Supervisor of cons, null actors
	private ActorRef replyTo;				// Place to send result
	private MapRequestMessage request;		// Original request, containing function to apply
	private int computed;
	
	public MapProcessingActor(ActorRef listManager, ActorRef replyTo, MapRequestMessage request) {
		this.listManager = listManager;
		this.replyTo = replyTo;
		this.request = request;
	}
	
	@Override
	public void onReceive(Object msg) throws Exception {
		if (msg instanceof StartProcessingMessage){
			ActorRef list = request.getArgumentListActor();
			list.tell(new NullStatusRequestMessage(), this.getSelf());
		}else if(msg instanceof NullStatusResultMessage){
			NullStatusResultMessage nullStatusRequest = (NullStatusResultMessage) msg;
			if(nullStatusRequest.getStatus() == true){
				this.listManager.tell(new NullActorRequestMessage(), this.getSelf());
			}else{
				// list is a ConsActor
				ActorRef consNode = this.request.getArgumentListActor();
				consNode.tell(new HeadRequestMessage(), this.getSelf());
				consNode.tell(new TailRequestMessage(), this.getSelf());
			}
		}else if(msg instanceof NullActorResultMessage){
			ActorRef nullActor = ((NullActorResultMessage) msg).getNullActor();
			this.replyTo.tell(new MapResultMessage(nullActor), this.getSelf());
		}else if(msg instanceof ConsActorResultMessage){
			ActorRef consActor = ((ConsActorResultMessage) msg).getConsActor();
			this.replyTo.tell(new MapResultMessage(consActor), this.getSelf());
		}else if(msg instanceof HeadResultMessage){
			int headVal = ((HeadResultMessage) msg).getHead();
			this.computed = this.request.getIntegerCompute().compute(headVal);
		}else if(msg instanceof TailResultMessage){
			ActorRef tail = ((TailResultMessage) msg).getTail();
			MapRequestMessage newRequest = new MapRequestMessage(this.request, tail);
			this.listManager.tell(newRequest, this.getSelf());
		}else if(msg instanceof MapResultMessage){
			this.listManager.tell(new ConsActorRequestMessage(this.computed, ((MapResultMessage) msg).getResult()), this.getSelf()); 
		}else{
			System.out.printf("Bad message to ListManagerActor:  %s%n", msg.toString());
			unhandled(msg);  // Recommended practice
		}
	}

}

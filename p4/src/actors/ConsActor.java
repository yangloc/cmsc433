package actors;

import java.util.ArrayList;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;

import messages.CollectRequestMessage;
import messages.CollectResultMessage;
import messages.HeadRequestMessage;
import messages.HeadResultMessage;
import messages.NullStatusRequestMessage;
import messages.NullStatusResultMessage;
import messages.TailRequestMessage;
import messages.TailResultMessage;
import support.IntegerCollector;

/**
 * Class of actors corresponding to cons ("internal") nodes in applicative lists.  Each
 * such actor stores a head field and a tail field; the later is a reference to an actor
 * that manages the rest of the list.
 * 
 * You must complete the implementation of onReceive().  You also add additional private fields if
 * you wish, although you may not change any constructor.
 *
 */
public class ConsActor extends UntypedActor {
	
	/**
	 * Props structure-generator for this class.
	 * 
	 * @param head	Head of the list the new node will manage
	 * @param tail	Rest of the lsit
	 * @return		Props structure for creating actor
	 */
	static Props props(Integer head, ActorRef tail) {
		return Props.create(ConsActor.class, head, tail);
	}
	
	private final Integer head;		// Head of list being managed by cons actor
	private final ActorRef tail;	// Actor managing tail of this list
	
	public ConsActor(Integer head, ActorRef tail) {
		this.head = head;
		this.tail = tail;
	}

	@Override
	public void onReceive(Object msg) throws Exception {
		if (msg instanceof NullStatusRequestMessage){
			getSender().tell(new NullStatusResultMessage(false), getSelf());
		}else if(msg instanceof HeadRequestMessage){
			getSender().tell(new HeadResultMessage(this.head), getSelf());
		}else if(msg instanceof TailRequestMessage){
			getSender().tell(new TailResultMessage(this.tail), getSelf());
		}else if(msg instanceof CollectRequestMessage){
			CollectRequestMessage<ArrayList<Integer>> request = (CollectRequestMessage<ArrayList<Integer>>) msg;
			IntegerCollector<ArrayList<Integer>> collector = request.getIntegerCollector();
			collector.collect(head);
			this.tail.forward(request, this.getContext());
		}else{
			System.out.printf("Bad message to ListManagerActor:  %s%n", msg.toString());
			unhandled(msg);  // Recommended practice
		}
	}

}

package cmsc433.p2;

import java.util.ArrayList;
import java.util.TreeMap;

/*
 *  Weiyanli Chen
 * 
 * */

/**
 * A Machine is used to make a particular Food.  Each Machine makes
 * just one kind of Food.  Each machine has a capacity: it can make
 * that many food items in parallel; if the machine is asked to
 * produce a food item beyond its capacity, the requester blocks.
 * Each food item takes at least item.cookTimeS seconds to produce.
 */

public class Machine {
	
	// Types of machines used in Ratsie's.  Recall that enum types are
	// effectively "static" and "final", so each instance of Machine
	// will use the same MachineType.
	
	public enum MachineType { fountain, fryer, grillPress, oven };
	
	// Converts Machine instances into strings based on MachineType.
	
	public String toString() {
		switch (machineType) {
		case fountain: 		return "Fountain";
		case fryer:			return "Fryer";
		case grillPress:	return "Grill Presss";
		case oven:			return "Oven";
		default:			return "INVALID MACHINE";
		}
	}
	
	public final MachineType machineType;
	public final Food machineFoodType;
	public final TreeMap<String, Food> foodToCook;
	public Object machineLock = new Object();
	public int capacity;

	//YOUR CODE GOES HERE...
	public boolean machineAvailable(){
		return this.foodToCook.size() < this.capacity;
	}

	public int machineAvailableSlots(){
		return this.capacity - this.foodToCook.size();
	}
	
	/**
	 * The constructor takes at least the type of the machine,
	 * the Food item it makes, and its capacity.  You may extend
	 * it with other arguments, if you wish.  Notice that the
	 * constructor currently does nothing with the capacity; you
	 * must add code to make use of this field (and do whatever
	 * initialization etc. you need).
	 */
	public Machine(MachineType machineType, Food food, int capacityIn) {
		this.machineType = machineType;
		this.machineFoodType = food;
		this.capacity = capacityIn;
		this.foodToCook = new TreeMap<String, Food>();
	}

	/**
	 * This method is called by a Cook in order to make the Machine's
	 * food item.  You can extend this method however you like, e.g.,
	 * you can have it take extra parameters or return something other
	 * than Object.  It should block if the machine is currently at full
	 * capacity.  If not, the method should return, so the Cook making
	 * the call can proceed.  You will need to implement some means to
	 * notify the calling Cook when the food item is finished.
	 */
	public void makeFood(Cook cook, Food food, int foodId) throws InterruptedException {
		// Was guarded by machineLock when called.
		//YOUR CODE GOES HERE...
		Thread newCookingTask = new Thread(new CookAnItem(cook, food, foodId, this));
		synchronized(this.machineLock){
			while(!this.machineAvailable()) this.machineLock.wait();
			// let me in!
			this.foodToCook.put(cook.toString() + " " + foodId, food);
			newCookingTask.start();
			Simulation.logEvent(SimulationEvent.cookStartedFood(cook, food, cook.getCurrOrderInfo().getOrderId()));
		}
		newCookingTask.join();
	}

	//THIS MIGHT BE A USEFUL METHOD TO HAVE AND USE BUT IS JUST ONE IDEA
	private class CookAnItem implements Runnable {
		private Cook cook;
		private Machine _self;
		private Food foodItem;
		private int foodId;
		
		public CookAnItem(Cook cook, Food food, int foodId, Machine thisMachine){
			this.foodItem = food;
			this.cook = cook;
			this.foodId = foodId;
			_self = thisMachine;
		}
		
		public void run() {
			try {
				//YOUR CODE GOES HERE...
				// Cooking...
				Simulation.logEvent(SimulationEvent.machineCookingFood(_self, foodItem));
				Thread.sleep(this.foodItem.cookTimeS);
				// Ding!
				synchronized(_self.machineLock){
					_self.foodToCook.remove(cook.toString() + " " + foodId);
					_self.machineLock.notify();
				}
				Simulation.logEvent(SimulationEvent.machineDoneFood(_self, foodItem));
				Simulation.logEvent(SimulationEvent.cookFinishedFood(cook, foodItem, cook.getCurrOrderInfo().getOrderId()));

				synchronized(cook.cookOrderLock) {
					cook.currOrderFoodStatus.put(this.foodId, true);
					cook.cookOrderLock.notify();
				}
				
				
			} catch(InterruptedException e) { e.printStackTrace();}
		}
	}
}
package cmsc433.p2;

import java.util.List;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.TreeMap;

/*
 *  Weiyanli Chen
 * 
 * */

/**
 * Cooks are simulation actors that have at least one field, a name.
 * When running, a cook attempts to retrieve outstanding orders placed
 * by Eaters and process them.
 */
public class Cook implements Runnable {
	private final String name;
	private Order currOrderInfo;
	private List<Food> currOrderFood;
	public TreeMap<Integer, Food> currOrderFoodIds;
	public TreeMap<Integer, Boolean> currOrderFoodPutIntoMachine;
	public TreeMap<Integer, Boolean> currOrderFoodStatus;
	public volatile TreeMap<String, Integer> uncookedFoodCounts;
	public final Object cookOrderLock = new Object();
	/**
	 * You can feel free to modify this constructor.  It must
	 * take at least the name, but may take other parameters
	 * if you would find adding them useful. 
	 *
	 * @param: the name of the cook
	 */
	public Cook(String name) {
		this.name = name;
	}

	public Order getCurrOrderInfo() {
		return currOrderInfo;
	}

	public String toString() {
		return name;
	}

	public boolean allFoodReady(){
		for(Boolean foodStatus : this.currOrderFoodStatus.values()) {
			if(foodStatus == false) return false; 
		}
		return true;
	}

	/**
	 * This method executes as follows.  The cook tries to retrieve
	 * orders placed by Customers.  For each order, a List<Food>, the
	 * cook submits each Food item in the List to an appropriate
	 * Machine, by calling makeFood().  Once all machines have
	 * produced the desired Food, the order is complete, and the Customer
	 * is notified.  The cook can then go to process the next order.
	 * If during its execution the cook is interrupted (i.e., some
	 * other thread calls the interrupt() method on it, which could
	 * raise InterruptedException if the cook is blocking), then it
	 * terminates.
	 */
	public void run() {

		Simulation.logEvent(SimulationEvent.cookStarting(this));
		this.currOrderFoodIds = new TreeMap<Integer, Food>();
		this.currOrderFoodStatus = new TreeMap<Integer, Boolean>();
		this.uncookedFoodCounts = new TreeMap<String, Integer>();

		try {
			while(true) {
				//YOUR CODE GOES HERE...
				synchronized(Simulation.orderingLock){
					while(!Simulation.hasOrderToTake()) Simulation.orderingLock.wait();

					this.currOrderInfo = Simulation.orders.poll();
					this.currOrderFood = currOrderInfo.getFoodList();
					Simulation.logEvent(SimulationEvent.cookReceivedOrder(this, this.currOrderFood, this.currOrderInfo.getOrderId()));
				}

				// Preliminary Work
				for (int i = 0; i < this.currOrderFood.size(); i++) {
					Food currFood = this.currOrderFood.get(i);

					this.currOrderFoodIds.put(i, currFood);
					this.currOrderFoodStatus.put(i, false);

					// Count foods
					if(!this.uncookedFoodCounts.containsKey(currFood.toString())) this.uncookedFoodCounts.put(currFood.toString(), 1);
					else this.uncookedFoodCounts.put(currFood.toString(), this.uncookedFoodCounts.get(currFood.toString()) + 1);
				}

				// Put food
				this.cookOrder();

				// Wait for food
				synchronized(this.cookOrderLock){
					while(!allFoodReady()) this.cookOrderLock.wait();
				}

				// Food is done:
				// 1. notfy the user to go home
				// 2. clean up all current order info
				synchronized(this.currOrderInfo.getOrderLock()) {
					//Simulation.orders.remove(this.currOrderInfo.getOrderId());
					this.currOrderInfo.foodReady = true;
					Simulation.logEvent(SimulationEvent.cookCompletedOrder(this, this.currOrderInfo.getOrderId()));
					this.currOrderInfo.getOrderLock().notify();
				}
				this.currOrderFoodIds.clear();
				this.currOrderFoodStatus.clear();
				this.currOrderInfo = null;

			}
		}
		catch(InterruptedException e) {
			// This code assumes the provided code in the Simulation class
			// that interrupts each cook thread when all customers are done.
			// You might need to change this if you change how things are
			// done in the Simulation class.
			Simulation.logEvent(SimulationEvent.cookEnding(this));
		}
	}

	private void cookOrder() throws InterruptedException{
			PriorityQueue<Integer> idList = new PriorityQueue<Integer>();
			while(!this.uncookedFoodCounts.isEmpty()){
				Entry<String, Integer> foodtypeCount = this.uncookedFoodCounts.firstEntry();
				idList.clear();
				String currFoodType = foodtypeCount.getKey();
				Food currFoodRef = null;
				// collect food ids
				for(Entry<Integer, Food> idFood : this.currOrderFoodIds.entrySet()){
					if(idFood.getValue().toString().equals(currFoodType.toString())) {
						idList.add(idFood.getKey());
						currFoodRef = idFood.getValue();
					}
				}
				// put put
				while(!idList.isEmpty()){
					this.putFoodInMachine(currFoodRef, idList.poll());
				}
				this.uncookedFoodCounts.remove(currFoodType);
			}
	}

	private void putFoodInMachine(Food food, int foodId) throws InterruptedException{
		if(food.toString().equals("wings")) {
			Simulation.fryer.makeFood(this, food, foodId);
		}else if(food.toString().equals("pizza")) {
			Simulation.oven.makeFood(this, food, foodId);
		}else if(food.toString().equals("sub")) {
			Simulation.grillPress.makeFood(this, food, foodId);
		}else if(food.toString().equals("soda")) {
			Simulation.fountain.makeFood(this, food, foodId);
		}else{
			throw new IllegalArgumentException("We don't sell this item in Ratsie's!");
		}
	}
}
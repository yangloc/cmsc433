package cmsc433.p2;

import java.util.List;

/*
 *  Weiyanli Chen
 * 
 * */

public class Order implements Comparable<Order>{
	private List<Food> foodList;
	private Object orderLock;
	private int orderId;
	public boolean foodReady;
	
	public List<Food> getFoodList() {
		return foodList;
	}

	public Object getOrderLock() {
		return orderLock;
	}

	public int getOrderId() {
		return orderId;
	}

	public Order(List<Food> foodList, Object lock, int orderId){
		this.foodList = foodList;
		this.orderLock = lock;
		this.orderId = orderId;
		this.foodReady = false;
	}

	@Override
	public int compareTo(Order order2) {
		
		return new Integer(this.orderId).compareTo(order2.orderId);
	}
}

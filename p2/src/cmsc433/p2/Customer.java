package cmsc433.p2;

import java.util.List;
import cmsc433.p2.SimulationEvent;
import cmsc433.p2.Simulation;
import cmsc433.p2.Order;

/*
 *  Weiyanli Chen
 * 
 * */

/**
 * Customers are simulation actors that have two fields: a name, and a list
 * of Food items that constitute the Customer's order.  When running, an
 * customer attempts to enter the Ratsie's (only successful if the
 * Ratsie's has a free table), place its order, and then leave the
 * Ratsie's when the order is complete.
 */
public class Customer implements Runnable {
	//JUST ONE SET OF IDEAS ON HOW TO SET THINGS UP...
	private final String name;
	private final List<Food> order;
	private final int orderNum;
	public final Order orderInfo;
	private final Object customerOrderLock = new Object();

	private static int runningCounter = 0;

	/**
	 * You can feel free modify this constructor.  It must take at
	 * least the name and order but may take other parameters if you
	 * would find adding them useful.
	 */
	public Customer(String name, List<Food> order) {
		this.name = name;
		this.order = order;
		this.orderNum = runningCounter++;
		this.orderInfo = new Order(order, customerOrderLock, orderNum);
	}

	public String toString() {
		return name;
	}

	/** 
	 * This method defines what an Customer does: The customer attempts to
	 * enter the Ratsie's (only successful when the Ratsie's has a
	 * free table), place its order, and then leave the Ratsie's
	 * when the order is complete.
	 */
	public void run() {
		//YOUR CODE GOES HERE...
		/* Before entering the Ratsie's: SimulationEvent.customerStarting()
		  - After entering the Ratsie's:
		  SimulationEvent.customerEnteredRatsies()
		  - After placing order (but before it has been filled):
		  SimulationEvent.customerPlacedOrder()
		  - After receiving order: SimulationEvent.customerReceivedOrder()
		  - Just before leaving the Ratsie's:
		  SimulationEvent.customerLeavingRatsies()
		 * */
		SimulationEvent.customerStarting(this);

		try {
			// Enter the Ratsie's
			synchronized(Simulation.customerCountLock){
				while(!Simulation.seatsAvailable()) Simulation.customerCountLock.wait(); // Customer first wait to be seated

				Simulation.logEvent(SimulationEvent.customerEnteredRatsies(this));
				Simulation.customerCount++;
			}
			// Place order
			synchronized(Simulation.orderingLock){
				Simulation.orders.add(this.orderInfo);
				Simulation.logEvent(SimulationEvent.customerPlacedOrder(this, this.order, this.orderNum));
				Simulation.orderingLock.notify();
			}
			// Wait for food
			synchronized(this.customerOrderLock){
				while(!this.orderInfo.foodReady) this.customerOrderLock.wait();
				Simulation.logEvent(SimulationEvent.customerReceivedOrder(this, this.order, this.orderNum));
			}
			// Get outta here
			synchronized(Simulation.customerCountLock){
				Simulation.customerCount--;
				Simulation.customerCountLock.notify();
				Simulation.logEvent(SimulationEvent.customerLeavingRatsies(this));
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}
}
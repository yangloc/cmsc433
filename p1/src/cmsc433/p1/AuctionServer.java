package cmsc433.p1;

/**
 *  @author YOUR NAME SHOULD GO HERE
 */


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;



public class AuctionServer
{
	/**
	 * Singleton: the following code makes the server a Singleton. You should
	 * not edit the code in the following noted section.
	 * 
	 * For test purposes, we made the constructor protected. 
	 */

	/* Singleton: Begin code that you SHOULD NOT CHANGE! */
	protected AuctionServer()
	{
	}

	private static AuctionServer instance = new AuctionServer();

	public static AuctionServer getInstance()
	{
		return instance;
	}

	/* Singleton: End code that you SHOULD NOT CHANGE! */





	/* Statistic variables and server constants: Begin code you should likely leave alone. */


	/**
	 * Server statistic variables and access methods:
	 */
	private int soldItemsCount = 0;
	private int revenue = 0;

	public int soldItemsCount()
	{
		return this.soldItemsCount;
	}

	public int revenue()
	{
		return this.revenue;
	}



	/**
	 * Server restriction constants:
	 */
	public static final int maxBidCount = 10; // The maximum number of bids at any given time for a buyer.
	public static final int maxSellerItems = 20; // The maximum number of items that a seller can submit at any given time.
	public static final int serverCapacity = 80; // The maximum number of active items at a given time.


	/* Statistic variables and server constants: End code you should likely leave alone. */



	/**
	 * Some variables we think will be of potential use as you implement the server...
	 */

	// List of items currently up for bidding (will eventually remove things that have expired).
	private List<Item> itemsUpForBidding = new ArrayList<Item>();


	// The last value used as a listing ID.  We'll assume the first thing added gets a listing ID of 0.
	private int lastListingID = -1; 

	// List of item IDs and actual items.  This is a running list with everything ever added to the auction.
	private HashMap<Integer, Item> itemsAndIDs = new HashMap<Integer, Item>();

	// List of itemIDs and the highest bid for each item.  This is a running list with everything ever added to the auction.
	private HashMap<Integer, Integer> highestBids = new HashMap<Integer, Integer>();

	// List of itemIDs and the person who made the highest bid for each item.   This is a running list with everything ever bid upon.
	private HashMap<Integer, String> highestBidders = new HashMap<Integer, String>();




	// List of sellers and how many items they have currently up for bidding.
	private HashMap<String, Integer> itemsPerSeller = new HashMap<String, Integer>();

	// List of buyers and how many items on which they are currently bidding.
	private HashMap<String, Integer> itemsPerBuyer = new HashMap<String, Integer>();



	// Object used for instance synchronization if you need to do it at some point 
	// since as a good practice we don't use synchronized (this) if we are doing internal
	// synchronization.
	//

	// Lock for item ops
	private Object itemsLock = new Object(); 

	//Lock for bid ops
	private Object bidsLock = new Object();








	/*
	 *  The code from this point forward can and should be changed to correctly and safely 
	 *  implement the methods as needed to create a working multi-threaded server for the 
	 *  system.  If you need to add Object instances here to use for locking, place a comment
	 *  with them saying what they represent.  Note that if they just represent one structure
	 *  then you should probably be using that structure's intrinsic lock.
	 */


	/**
	 * Attempt to submit an <code>Item</code> to the auction
	 * @param sellerName Name of the <code>Seller</code>
	 * @param itemName Name of the <code>Item</code>
	 * @param lowestBiddingPrice Opening price
	 * @param biddingDurationMs Bidding duration in milliseconds
	 * @return A positive, unique listing ID if the <code>Item</code> listed successfully, otherwise -1
	 */
	@SuppressWarnings("unused")
	public int submitItem(String sellerName, String itemName, int lowestBiddingPrice, int biddingDurationMs)
	{
		// TODO: IMPLEMENT CODE HERE
		// Some reminders:
		//   Make sure there's room in the auction site.
		//   If the seller is a new one, add them to the list of sellers.
		//   If the seller has too many items up for bidding, don't let them add this one.
		//   Don't forget to increment the number of things the seller has currently listed.

		boolean success = false;
		int successItemId;
		synchronized (this.itemsLock){
			if(maxSellerItems == 0){
				// When the auction is FAKE.
				return -1;
			}else if(this.itemsUpForBidding.size() == serverCapacity){
				// When the auction site has no room
				return -1;
			}else if(!this.itemsPerSeller.containsKey(sellerName)){
				// A new seller
				int itemId = ++lastListingID;
				successItemId = itemId;
				Item currItem = new Item(sellerName, itemName, itemId, lowestBiddingPrice, biddingDurationMs);
				this.itemsUpForBidding.add(currItem);
				this.itemsAndIDs.put(itemId, currItem);

				//TODO complete
				this.itemsPerSeller.put(sellerName, 1);
				success = true;
			}else if(this.itemsPerSeller.get(sellerName) == maxSellerItems){
				// Show me the money? NO.
				return -1;
			}else{ //Success
				int itemId = ++lastListingID;
				int numSellerItems = this.itemsPerSeller.get(sellerName);
				successItemId = itemId;
				Item currItem = new Item(sellerName, itemName, itemId, lowestBiddingPrice, biddingDurationMs);
				this.itemsUpForBidding.add(currItem);
				this.itemsAndIDs.put(itemId, currItem);
				//TODO 
				this.itemsPerSeller.put(sellerName, ++numSellerItems);
				success = true;
			}
		}

		return successItemId;
	}



	/**
	 * Get all <code>Items</code> active in the auction
	 * @return A copy of the <code>List</code> of <code>Items</code>
	 */
	public List<Item> getItems()
	{
		// Some reminders:
		//    Don't forget that whatever you return is now outside of your control.
		return new ArrayList<Item>(this.itemsUpForBidding);
	}


	/**
	 * Attempt to submit a bid for an <code>Item</code>
	 * @param bidderName Name of the <code>Bidder</code>
	 * @param listingID Unique ID of the <code>Item</code>
	 * @param biddingAmount Total amount to bid
	 * @return True if successfully bid, false otherwise
	 */
	public boolean submitBid(String bidderName, int listingID, int biddingAmount)
	{
		// TODO: IMPLEMENT CODE HERE
		// Some reminders:
		//   See if the item exists.
		//   See if it can be bid upon.
		//   See if this bidder has too many items in their bidding list.
		//   Get current bidding info.
		//   See if they already hold the highest bid.
		//   See if the new bid isn't better than the existing/opening bid floor.
		//   Decrement the former winning bidder's count
		//   Put your bid in place
		boolean result = false;
		synchronized(this.itemsLock){
			if(this.itemsAndIDs.get(listingID)== null ||
					!this.itemsAndIDs.get(listingID).biddingOpen())
				result = false;
			else{
				if(this.itemsPerBuyer.get(bidderName) == null){
					// Bidder hasn't put any bids
					synchronized(this.bidsLock){
						result = this.submitBidHelper(bidderName, listingID, biddingAmount);
					}
				}else if(this.itemsPerBuyer.get(bidderName) >= maxBidCount){
					result = false;
				}else{
					synchronized(this.bidsLock){
						result = this.submitBidHelper(bidderName, listingID, biddingAmount);
					}
				}
			}
		}

		return result;
	}

	//   Get current bidding info.
	//   See if they already hold the highest bid.
	//   See if the new bid isn't better than the existing/opening bid floor.
	//   Decrement the former winning bidder's count
	//   Put your bid in place
	private boolean submitBidHelper(String bidderName, int listingID, int biddingAmount){
		boolean result = false;
		Integer highestBid = this.highestBids.get(listingID);
		String highestBidderName = this.highestBidders.get(listingID);
		if(highestBid == null || highestBidderName == null){
			// The item is not bidded yet.
			this.highestBids.put(listingID, biddingAmount);
			this.highestBidders.put(listingID, bidderName);
			int newHighBidderCount = itemsPerBuyer.get(bidderName) == null? 1 : itemsPerBuyer.get(bidderName) + 1;
			this.itemsPerBuyer.put(bidderName, newHighBidderCount);
			result = true;
		}
		else if(highestBidderName.equals(bidderName) ||
				highestBid >= biddingAmount)
			result = false;
		else{
			// Bidding succeeds
			this.highestBids.put(listingID, biddingAmount);
			this.itemsPerBuyer.put(highestBidderName, itemsPerBuyer.get(highestBidderName) - 1);
			this.highestBidders.put(listingID, bidderName);
			int newHighBidderCount = itemsPerBuyer.get(bidderName) == null? 1 : itemsPerBuyer.get(bidderName) + 1;
			this.itemsPerBuyer.put(bidderName, newHighBidderCount);
			result = true;
		}
		return result;
	}

	/**
	 * Check the status of a <code>Bidder</code>'s bid on an <code>Item</code>
	 * @param bidderName Name of <code>Bidder</code>
	 * @param listingID Unique ID of the <code>Item</code>
	 * @return 1 (success) if bid is over and this <code>Bidder</code> has won<br>
	 * 2 (open) if this <code>Item</code> is still up for auction<br>
	 * 3 (failed) If this <code>Bidder</code> did not win or the <code>Item</code> does not exist
	 */
	public int checkBidStatus(String bidderName, int listingID)
	{
		// TODO: IMPLEMENT CODE HERE
		// Some reminders:
		//   If the bidding is closed, clean up for that item.
		//     Remove item from the list of things up for bidding.
		//     Decrease the count of items being bid on by the winning bidder if there was any...
		//     Update the number of open bids for this seller
		int result = 3;
		synchronized(this.itemsLock){
			Item currItem = this.itemsAndIDs.get(listingID);
			// If the item does not exist ever
			if(currItem == null){
				result = 3;
			}else if(!currItem.biddingOpen()){
				String currItemSeller = currItem.seller();
				synchronized(this.bidsLock){
					//If the item is closed for bidding but still "UP" in the list, do the cleanup.
					if(this.itemsUpForBidding.contains(currItem)){
//						int itemIndex = this.itemsUpForBidding.indexOf(currItem);
//						this.itemsUpForBidding.remove(i5temIndex);
						if(this.highestBids.get(listingID) != null){
							revenue += this.highestBids.get(listingID);
						}
						this.itemsUpForBidding.remove(currItem);
					}
					// if item is actually sold, ++ the sold count, -- itemsPerSeller
					if(this.highestBids.get(listingID) != null){
						this.soldItemsCount++;
						this.itemsPerSeller.put(currItemSeller, itemsPerSeller.get(currItemSeller) - 1);
//						revenue += this.highestBids.get(listingID);
						if(this.highestBidders.get(listingID).equals(bidderName)){
							//if the bidder WIN!
							int bidderItemCount = this.itemsPerBuyer.get(bidderName);
							this.itemsPerBuyer.put(bidderName, bidderItemCount);
							result = 1;
						}else{
							// the item is won by somebody else.
							result = 3;
						}				
					}else{
						// nobody bids this item.
						result = 3;
					}
					this.itemsUpForBidding.remove(currItem);
				}
			}else{
				return 2;
			}
		}
		return result;
	}

	/**
	 * Check the current bid for an <code>Item</code>
	 * @param listingID Unique ID of the <code>Item</code>
	 * @return The highest bid so far or the opening price if no bid has been made,
	 * -1 if no <code>Item</code> exists
	 */
	public int itemPrice(int listingID)
	{
		synchronized(this.itemsLock){
			if(this.itemsAndIDs.get(listingID) == null){
				return -1;
			}else{
				synchronized(this.bidsLock){
					return this.highestBids.get(listingID) == null? 
							this.itemsAndIDs.get(listingID).lowestBiddingPrice() : 
							this.highestBids.get(listingID);
				}
			}

		}
	}

	/**
	 * Check whether an <code>Item</code> has been bid upon yet
	 * @param listingID Unique ID of the <code>Item</code>
	 * @return True if there is no bid or the <code>Item</code> does not exist, false otherwise
	 */
	public Boolean itemUnbid(int listingID)
	{
		synchronized(this.bidsLock){
			return (this.highestBids.get(listingID) == null)? true : false;
		}
	}


}

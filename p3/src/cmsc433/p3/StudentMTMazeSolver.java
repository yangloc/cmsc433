package cmsc433.p3;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.Collections;

/**
 * This file needs to hold your solver to be tested. 
 * You can alter the class to extend any class that extends MazeSolver.
 * It must have a constructor that takes in a Maze.
 * It must have a solve() method that returns the datatype List<Direction>
 *   which will either be a reference to a list of steps to take or will
 *   be null if the maze cannot be solved.
 */

public class StudentMTMazeSolver extends SkippingMazeSolver
{

	private volatile ExecutorService executor;
	private volatile LinkedBlockingDeque<DFSTask> workQueue;
	private int taskCount;
	private int numProcessors = Runtime.getRuntime().availableProcessors();

	public StudentMTMazeSolver(Maze maze)
	{
		super(maze);
	}

	public LinkedList<Direction> flipDirections(LinkedList<Direction> dirs){
		LinkedList<Direction> result = new LinkedList<Direction>();
		for(Direction d: dirs){
			result.add(d.reverse());
		}
		return result;
	} 

	public Choice firstChoiceR(Position pos) throws SolutionFound
	{
		LinkedList<Direction> moves;

		moves = maze.getMoves(pos);
		if (moves.size() == 1) return follow(pos, ((LinkedList<Direction>) moves).getFirst());
		else return new Choice(pos, null, moves);
	}

	public Choice followR(Position at, Direction dir, int color) throws SolutionFound
	{
		LinkedList<Direction> choices;
		Direction go_to = dir, came_from = dir.reverse();

		maze.setColor(at, color);
		at = at.move(go_to);
		do
		{
			maze.setColor(at, color);
			if (at.equals(maze.getEnd())) throw new SolutionFound(at, go_to.reverse());
			if (at.equals(maze.getStart())) throw new SolutionFound(at, go_to.reverse());
			choices = maze.getMoves(at);
			choices.remove(came_from);

			if (choices.size() == 1)
			{
				go_to = choices.getFirst();
				at = at.move(go_to);
				came_from = go_to.reverse();
			}
		} while (choices.size() == 1);
		// return new Choice(at,choices);
		Choice ret = new Choice(at, came_from, choices);
		return ret;
	}

	public List<Direction> solve() 
	{
//		executor = Executors.newFixedThreadPool(numProcessors);
		executor = ForkJoinPool.commonPool();
		workQueue = new LinkedBlockingDeque<DFSTask>();
		List<Future<List<Direction>>> taskResults = new LinkedList<Future<List<Direction>>>();
		List<Direction> result = null;
		taskCount = 0;
		try{
//			Choice startPtChoice = firstChoice(maze.getStart());
//			int numDirection = startPtChoice.choices.size();
//			for(int i = 0; i < numDirection; i++){
//				Choice currChoice = follow(startPtChoice.at, startPtChoice.choices.peek());
//				workQueue.add(new DFSTask(currChoice, startPtChoice.choices.pop()));
//				taskCount++;
//			}
			/* cannot beat single threaded DFS if enabled both. It turns out solving unsovable
			 * maze(sample input) faster if go from the end.
			 * */
			if(taskCount < numProcessors){
				int numReverseTask = numProcessors - taskCount;
				Choice endPtChoice = firstChoiceR(maze.getEnd());
				int numDirectionR = endPtChoice.choices.size();
				for(int i = 0; i <  numReverseTask && i < numDirectionR && taskCount < numProcessors; i++){
					Choice currChoice = followMark(endPtChoice.at, endPtChoice.choices.peek(), 3);
					workQueue.add(new DFSTask(currChoice, endPtChoice.choices.pop(), true));
					taskCount++;
				}
			}
		}catch (Exception e){
			// make complier happy
		}
		try {
			taskResults = executor.invokeAll(workQueue);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		} 
		executor.shutdown();
		ForkJoinPool.commonPool().awaitQuiescence(25, TimeUnit.SECONDS);
		for(Future<List<Direction>> taskResult : taskResults){
			try {
				if(taskResult.get() != null){
					result = taskResult.get();
					return result;
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

    public List<Direction> pathToFullPathR(List<Direction> path)
    {
        Iterator<Direction> pathIter = path.iterator();
        LinkedList<Direction> fullPath = new LinkedList<Direction>();

        // Get full solution path.
        Position curr = maze.getEnd();
        Direction go_to = null, came_from = null;
        while (!curr.equals(maze.getStart()))
        {
            LinkedList<Direction> moves = maze.getMoves(curr);
            moves.remove(came_from);
            if (moves.size() == 1) go_to = moves.getFirst();
            else if (moves.size() > 1) go_to = pathIter.next();
            else if (moves.size() == 0)
            {
                System.out.println("Error in solution--leads to deadend.");
                throw (new Error());
                // System.exit(-1);
            }
            fullPath.add(go_to);
            curr = curr.move(go_to);
            came_from = go_to.reverse();
        }
        Collections.reverse(fullPath);
        fullPath = (LinkedList<Direction>) this.flipDirections(fullPath);
        return fullPath;
    }
	
	private class DFSTask implements Callable<List<Direction>>{
		private Choice ch;
		private Direction dir;
		private boolean isReversed;

		public DFSTask(Choice head, Direction choiceDir){
			this.ch = head;
			this.dir = choiceDir;
			this.isReversed = false;
		}

		public DFSTask(Choice head, Direction choiceDir, boolean isReversed){
			this.ch = head;
			this.dir = choiceDir;
			this.isReversed = isReversed;
		}

		@Override
		public List<Direction> call() {
			LinkedList<Choice> choiceStack = new LinkedList<Choice>();
			Choice curr;
			try{
				choiceStack.push(this.ch);

				while(!choiceStack.isEmpty()){
					curr = choiceStack.peek();

					if(curr.isDeadend()){
						//backtrack
						choiceStack.pop();
						if (!choiceStack.isEmpty()) choiceStack.peek().choices.pop();
						continue;
					}
						choiceStack.push(follow(curr.at, curr.choices.peek()));
				}
				return null;
			}catch (SolutionFound e){
				Iterator<Choice> iter = choiceStack.iterator();
				LinkedList<Direction> solutionPath = new LinkedList<Direction>();
				while (iter.hasNext())
				{
					curr = iter.next();
					solutionPath.push(curr.choices.peek());
				}
				solutionPath.push(dir);
				if (maze.display != null) maze.display.updateDisplay();
				if (this.isReversed){
					return pathToFullPathR(solutionPath);
				}
				return pathToFullPath(solutionPath);
			}
		}
	}
}
